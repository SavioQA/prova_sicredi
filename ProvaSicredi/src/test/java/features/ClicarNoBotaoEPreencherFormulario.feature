#language: pt
#encoding: utf-8

  Funcionalidade: Switch theme
    #Eu Como Candidato
    #Quero Mudar o valor da combo Select version para “Bootstrap V4 Theme”
    #Para Preencher os campos do formulário

    Esquema do Cenario: Mudar o valor da combo Select version para “Bootstrap V4 Theme” e Preencher os campos do formulário
      Dado que esteja na site www.grocerycrud.com
      Quando clica em Switch theme e muda o valor da combo Select version para Bootstrap V4 Theme
      E clica no botao Add Record
      E preenche os campos CustomerName <nome> e ContactLastName <sobrenome>
      E preenche o campo ContactFirstName <pNome> e Phone <fone>
      E preenche o campo AddressLine1 <endereco> e AddressLine2 <endereco2>
      E preenche o campo City <cidade> e State <estado>
      E preenche o campo PostalCode <codigoPostal> e Country <pais>
      E preenche o campo SalesRepEmployeeNumber <numeroRepresentanteVendas>
      E preenche o campo CreditLimit <creditoLimite>
      E clica em Save
      Entao deve-se visualizar <tipoCenario>

      Exemplos:
        | tipoCenario            | nome          | sobrenome | pNome         | fone       | endereco              | endereco2 | cidade       | estado | codigoPostal | pais   | numeroRepresentanteVendas | creditoLimite |
        |"Preenchido com sucesso"|"Teste Sicredi"|"Teste"    |"Savio Holanda"|"5199999999"|"Av Assis Brasil, 3970"|"Torre D"  |"Porto Alegre"|"RS"    |"91000-000"   |"Brasil"|"1"                        |"200"          |
