package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.E;
import io.cucumber.java.it.Quando;
import io.cucumber.java.pt.Entao;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class ClicarNoBotaoEPreencherFormularioStep {

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void config() {
        System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        driver.manage().window().maximize();
    }

    @Dado("que esteja na site www.grocerycrud.com")
    public void que_esteja_na_site_www_grocerycrud_com() {
        String url = "https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap";
        driver.get(url);
    }

    @Quando("clica em Switch theme e muda o valor da combo Select version para Bootstrap V4 Theme")
    public void clica_em_switch_theme_e_muda_o_valor_da_combo_select_version_para_bootstrap_v4_theme() {
        driver.findElement(By.id("switch-version-select")).sendKeys(Keys.ENTER);
        driver.findElement(By.id("switch-version-select")).sendKeys("Bootstrap V4 Theme");
        driver.findElement(By.id("switch-version-select")).sendKeys(Keys.ENTER);
    }

    @E("clica no botao Add Record")
    public void clica_no_botao_add_record() {
        driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div[2]/form/div[1]/div[1]/a")).click();
    }

    @E("preenche os campos CustomerName {string} e ContactLastName {string}")
    public void preenche_os_campos_customer_name_e_contact_last_name(String nome, String sobrenome) {
        wait.until(ExpectedConditions.elementToBeClickable(By.id("field-customerName")));
        driver.findElement(By.id("field-customerName")).sendKeys(nome);
        driver.findElement(By.id("field-contactLastName")).sendKeys(sobrenome);
    }

    @E("preenche o campo ContactFirstName {string} e Phone {string}")
    public void preenche_o_campo_contact_first_name_e_phone(String pNome, String fone) {
        driver.findElement(By.id("field-contactFirstName")).sendKeys(pNome);
        driver.findElement(By.id("field-phone")).sendKeys(fone);
    }

    @E("preenche o campo AddressLine1 {string} e AddressLine2 {string}")
    public void preenche_o_campo_address_line1_e_address_line2(String endereco, String endereco2) {
        driver.findElement(By.id("field-addressLine1")).sendKeys(endereco);
        driver.findElement(By.id("field-addressLine2")).sendKeys(endereco2);
    }

    @E("preenche o campo City {string} e State {string}")
    public void preenche_o_campo_city_e_state(String cidade, String estado) {
        driver.findElement(By.id("field-city")).sendKeys(cidade);
        driver.findElement(By.id("field-state")).sendKeys(estado);
    }

    @E("preenche o campo PostalCode {string} e Country {string}")
    public void preenche_o_campo_postal_code_e_country(String codigoPostal, String pais) {
        driver.findElement(By.id("field-postalCode")).sendKeys(codigoPostal);
        driver.findElement(By.id("field-country")).sendKeys(pais);
    }

    @E("preenche o campo SalesRepEmployeeNumber {string}")
    public void preenche_o_campo_sales_rep_employee_number(String numeroRepresentanteVendas) {
        driver.findElement(By.id("field-salesRepEmployeeNumber")).sendKeys(numeroRepresentanteVendas);
    }

    @E("preenche o campo CreditLimit {string}")
    public void preenche_o_campo_credit_limit(String creditoLimite) {
        driver.findElement(By.id("field-creditLimit")).sendKeys(creditoLimite);
    }

    @E("clica em Save")
    public void clica_em_save() {
        driver.findElement(By.id("form-button-save")).click();
    }

    @Entao("deve-se visualizar {string}")
    public void deve_se_visualizar(String tipoCenario) {

        if (tipoCenario.equals("Preenchido com sucesso")) {
            wait.until(ExpectedConditions.elementToBeClickable(By.id("report-success")));
            String resposta = driver.findElement(By.id("report-success")).getText();
            Assert.assertEquals("Your data has been successfully stored into the database. Edit Record or Go back to list", resposta);
        }

    }

    @After
    public void fechar() {
        driver.quit();
    }

}
